
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Simone
 */
public class Pratica31 {
 public static void main(String[] args){
      Date inicio = new Date();
      String meuNome = "Simone Bello Kaminski Aires";
      System.out.println(meuNome.toUpperCase());
      System.out.println(meuNome.substring(22).toUpperCase()+", "+ meuNome.substring(0,1).toUpperCase()+". " + meuNome.substring(7,8).toUpperCase()+". "+meuNome.substring(13,14).toUpperCase()+".");
      GregorianCalendar dataNascimento = new GregorianCalendar(1977, Calendar.NOVEMBER, 22);
      Date hoje = new Date();
      Date dNasc = dataNascimento.getTime();
      long tempo1 = dNasc.getTime();  
      long tempo2 = hoje.getTime();  
      long ddias = tempo2 - tempo1;  
      long dias = ((ddias + 60L * 60 * 1000) / (24L * 60 * 60 * 1000)) + 1;
      System.out.println("Dias até hoje = "+ dias);
      Date fim = new Date();
      long tempo = fim.getTime() - inicio.getTime();
      System.out.println("Tempo decorrido = "+tempo);
    }   
}
